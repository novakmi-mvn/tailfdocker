package com.github.novakmi.tailfdocker.test

import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner
import org.testng.Assert
import org.testng.annotations.AfterMethod
import org.testng.annotations.BeforeMethod
import org.testng.annotations.Test

import java.lang.reflect.Method

//@Slf4j
class TailfDockerfileTaskTest {

    File testProjectDir
    private File settingsFile
    private File buildFile
    final String TAG="tailfdocker"

    @BeforeMethod(alwaysRun = true)
    public void setup(Method method) {
        testProjectDir = new File("${System.getProperty("testTmpDir")}/${method.getName()}")
        if (testProjectDir.exists()) {
            testProjectDir.deleteDir()
        }
        Assert.assertTrue testProjectDir.mkdirs()

        settingsFile = new File("${testProjectDir.getAbsolutePath()}/settings.gradle")
        buildFile = new File("${testProjectDir.getAbsolutePath()}/build.gradle")
        settingsFile.createNewFile()
        buildFile.createNewFile()
        settingsFile << "rootProject.name = '${method.getName()}'"
    }

    @AfterMethod(alwaysRun = true)
    public void deleteTempDir() {

    }

    @Test(groups = ["basic"])
    public void minimal() {
        buildFile <<
            "plugins {id 'tailfdocker'}\n" +
            "import com.github.novakmi.tailfdocker.TailfDockerfileTask\n" +
            "\n" +
            "task createConfDDevDockerfile(type: TailfDockerfileTask) {\n" +
            "   tag = getProp(\"IMAGE_TAG_LX\", \"$TAG\")\n" +
            "}\n"

        BuildResult result = GradleRunner.create()
            .withProjectDir(testProjectDir)
            .withPluginClasspath()
            .withArguments("createConfDDevDockerfile")
            .build()
        List lines = TestUtils.getDockerFileLines(testProjectDir, TAG)
        //Assert.assertEquals(lines.size(), 1) //one for last empty line
        Assert.assertTrue(TestUtils.contains(lines, "FROM ubuntu:22.04"))
    }

    @Test(groups = ["basic"])
    public void confd() {
        buildFile <<
            "plugins {id 'tailfdocker'}\n" +
            "import com.github.novakmi.tailfdocker.TailfDockerfileTask\n" +
            "\n" +
            "task createConfDDevDockerfile(type: TailfDockerfileTask) {\n" +
            "   tag = getProp(\"IMAGE_TAG_LX\", \"$TAG\")\n" +
            "   confd = [version: 6.7, package_dir:false]\n" + //package_dir false - do not copy ConfD files to context
            "}\n"

        BuildResult result = GradleRunner.create()
            .withProjectDir(testProjectDir)
            .withPluginClasspath()
            .withArguments("createConfDDevDockerfile")
            .build()

        List lines = TestUtils.getDockerFileLines(testProjectDir, TAG)
        //Assert.assertEquals(lines.size(), 13) //one for last empty line
        Assert.assertTrue(TestUtils.contains(lines, "FROM ubuntu:22.04"))
    }
}
