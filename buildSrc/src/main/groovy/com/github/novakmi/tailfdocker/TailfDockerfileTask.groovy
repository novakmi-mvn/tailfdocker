package com.github.novakmi.tailfdocker

import com.github.novakmi.lxdocker.LxDockerfileTask
import org.gradle.api.logging.Logging
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction
import org.slf4j.Logger

import java.util.concurrent.Callable

//CacheableTask
class TailfDockerfileTask extends LxDockerfileTask {
    private final static Logger logger = Logging.getLogger(TailfDockerfileTask)

    final static DEFAULT_CONFD_VERSION = "7.8"
    final static DEFAULT_NSO_VERSION = "5.8"

    /**
     * Gradle property or env. variable for default location of Tailf products installation
     * Default is: ${HOME}/tailf
     * @see package_dir property
     */
    final static PACKAGE_DIR_TF = "PACKAGE_DIR_TF"

    /**
     * Gradle property or env. variable for LUX installation from https://github.com/hawk/lux.git
     * Default is: false
     * @see installLux property
     */
    final static INSTALL_LUX_TF = "INSTALL_LUX_TF"

    /**
     * Gradle property or env. variable for default location of Tailf ConfD products installation
     * has higher priority then PACKAGE_DIR_TF
     * @see confd property (has higher priority)
     */
    final static CONFD_PACKAGE_DIR_TF = "CONFD_PACKAGE_DIR_TF"

    /**
     * Gradle property or env. variable for enabling/disabling installation of ConfD examples.
     * Default is true.
     * -PCONFD_EXAMPLES_TF=true
     * @see confdExamples  (has higher priority)
     * @see confd property (has highest priority)
     */
    final static CONFD_EXAMPLES_TF = "CONFD_EXAMPLES_TF"

    /**
     * Gradle property or env. variable for enabling/disabling installation of ConfD java package (since ConfD 7.3+).
     * Not applicable for ConfD Basic
     * Default is true.
     * -PCONFD_Java_TF=true
     * @see confdJava  (has higher priority)
     * @see confd property (has highest priority)
     */
    final static CONFD_JAVA_TF = "CONFD_JAVA_TF"

    /**
     * Gradle property or env. variable for enabling/disabling installation of ConfD doc.
     * Default is true.
     * -PCONFD_DOC_TF=true
     * @see confdDoc (has higher priority)
     * @see confd property (has highest priority)
     */
    final static CONFD_DOC_TF = "CONFD_DOC_TF"

    /**
     * Gradle property or env. variable specifying installation of ConfD basic (free) version.
     * Default is false.
     * -PCONFD_BASIC_TF=true
     * @see confd property (has highest priority)
     */
    final static CONFD_BASIC_TF = "CONFD_BASIC_TF"

    /**
     * Gradle property or env. variable forcing to add  xenial-security epository for libssl1.0.0
     * Default is false.
     * Note, this property may be removed when not needed (ConfD/NSO supports newer libssl)
     * -PXENIAL_SECURITY_TF=true
     * @see xenialSecurity property
     */
    final static XENIAL_SECURITY_TF = "XENIAL_SECURITY_TF"

    /**
     * Gradle property or env. variable specifying confd version.
     * Default is DEFAULT_CONFD_VERSION.
     * -PCONFD_VERSION_TF=6.8
     * @see DEFAULT_CONFD_VERSION
     * @see version property (has lower priority)
     * @see confd property (has highest priority)
     */
    final static CONFD_VERSION_TF = "CONFD_VERSION_TF"

    /**
     * Gradle property or env. variable for default location of Tailf NSO products installation
     * has higher priority then PACKAGE_DIR_TF
     * @see nso property (has higher priority)
     */
    final static NSO_PACKAGE_DIR_TF = "NSO_PACKAGE_DIR_TF"

    /**
     * Gradle property or env. variable specifying NSO version.
     * Default is DEFAULT_NSO_VERSION.
     * -PNSO_VERSION_TF=4.7
     * @see DEFAULT_NSO_VERSION* @see version property (has lower priority)
     * @see nso property (has highest priority)
     */
    final static NSO_VERSION_TF = "NSO_VERSION_TF"

    /**
     * Location where tailf applications are installed (for now, cannot be changed)
     */
    final static def tailfDir = "/opt/tailf"

    /**
     * Location where ConfD is installed (for now, cannot be changed)
     */
    final static def confdDir = "${tailfDir}/confd"

    /**
     * Location where NSO is installed (for now, cannot be changed)
     */
    final static def nsoDir = "${tailfDir}/nso"


    /**
     * Location where setup file is located (cannot be changed)
     */
    final static def setupTailfFile = "/etc/profile.d/setup-tailf.sh"

    /**
     * Package dependencies needed to run ConfD.
     * @see LxDockerfileTask.packages
     */
    @Input @org.gradle.api.tasks.Optional
    final Set confdRuntime = ["libssl1.1", "ssh", "libxml2-utils", "xmlstarlet"]

    /**
     * Package dependencies needed to install and run LUX.
     * @see LxDockerfileTask.packages
     */
    @Input @org.gradle.api.tasks.Optional
    final Set luxDeps = ["git", "autoconf", "build-essential", "erlang-base", "erlang-xmerl", "erlang-dev"]

    /**
     * Package dependencies needed to build and run ConfD JAVA apps.
     * @see LxDockerfileTask.packages
     */
    @Input @org.gradle.api.tasks.Optional
    final Set confdJavaRuntime = ["default-jdk"]

    /**
     * Package dependencies needed to run ConfD/NSO netconf-console.
     * @see LxDockerfileTask.packages
     */
    @Input @org.gradle.api.tasks.Optional
    final Set nconsoleRuntime = ["python3-paramiko"]

    /**
     * Package dependencies needed to run ConfD.
     * @see LxDockerfileTask.packages
     */
    @Input @org.gradle.api.tasks.Optional
    final Set nsoRuntime = confdRuntime + ["xsltproc"]

    /**
     * Package dependencies needed to build ConfD examples
     * @see LxDockerfileTask.packages
     */
    @Input @org.gradle.api.tasks.Optional
    final Set confdBuild = ["build-essential", "libssl-dev"]

    @Input
    def installLux = false

    /**
     * confd property is Map (can be empty map [:])
     * Map values have highest priority over property values or default values
     * Map elements (all optional):
     *    version: ConfD version to install
     *    package_dir: directory of ConfD installation package(s)
     *    arch: architecture (default is "x86_64")
     *    platform: platform (default is "linux")
     *    basic: use ConfD Basic (default is false)
     *    examples: install examples package (default is true)
          java: install java package (for ConfD 7.3+, default is false)
     *    localConfdConf: local confdConf file to use for ConfD
     *    doc: install doc package (default is true)
     *    ports:  list of ConfD ports to be exposed by docker container (default [2022, 2023, 2024, 4565, 8008, 8088])
     *    sshFix: do not fix SSH  keys (ssh needs to be fixed to use ConfD with OpenSSH 1.1.1+) - default false
     *    TODO libconfd: extract libconfd, compile with MAXPATH .... (not yet supported)
     * @see CONFD_VERSION_TF* @see PACKAGE_DIR_TF* @see CONFD_PACKAGE_DIR_TF* @see CONFD_BASIC_TF* @see CONFD_EXAMPLES_TF* @see CONFD_DOC_TF
     */
    @Input
    def confd = false

    /**
     * nso property is Map (can be empty map [:])
     * Map values have highest priority over property values or default values
     * Map elements (all optional):
     *    version: Nso version to install
     *    package_dir: directory of Nso installation package
     *    arch: architecture (default is "x86_64")
     *    platform: platform (default is "linux")
     *    ports:  list of NSO ports to be exposed by docker container (default [2022, 2023, 2024, 4565, 8008, 8088])
     *    sshFix: do not fix SSH  keys (ssh needs to be fixed to use ConfD with OpenSSH 1.1.1+) - default false
     * @see NSO_VERSION_TF* @see PACKAGE_DIR_TF* @see NSO_PACKAGE_DIR_TF
     */
    @Input
    def nso = false
    // true or map with info for confd installation [version: <ver> , package_dir: <dir>, arch: "x86_64",

    /**
     * xenialSecurity property specifies if xenial-security ubuntu repository is added for libssl1.0.0
     * default value is false
     * @see XENIAL_SECURITY_TF
     */
    @Input
    def xenialSecurity = getPropTrueFalse(XENIAL_SECURITY_TF, false)

    private def package_dir = getProp(PACKAGE_DIR_TF, "${System.getProperty('user.home')}/tailf")  // TODO make property
    private def confdExamples
    private def confdJava
    private def confdDoc

    /**
     * Pre-defined default command list representing launch of confd
     *
     * <pre>
     *  defaultCmd = defaultCmdConfD
     * </pre>
     *
     * @see defaultCmd
     */
    @Input @org.gradle.api.tasks.Optional
    final List defaultCmdConfD = ["${confdDir}/bin/confd", "--foreground", "-c", "${confdDir}/etc/confd/confd.conf"]

    /**
     * Pre-defined supervisor config for launch of confd
     *
     * <pre>
     *  supervisor = [supervisorConfD]
     * </pre>
     *
     * @see supervisor
     */
    @Input @org.gradle.api.tasks.Optional
    final Map supervisorConfD = [program: "confd", command: "${confdDir}/bin/confd -c ${confdDir}/etc/confd/confd.conf"]

    private def exampleToBuild = null
    private def exampleConfdConf = null

    /**
     * Return (confd.conf) slashed path with prefix for each element
     * @param pathList - list representing confd.conf path
     * @param prefix - prefix
     * @return string with confd.conf path
     */
    static def makePrefixedSlashPath(List pathList, String prefix) {
        String path = "/${prefix}:" + pathList.join("/${prefix}:")
        return path
    }

    /**
     * Edit (optionally into new copy) existing Conf file on remote
     * @param remoteConfFile
     * @param params map of xml path as key and value to be set, if xml path does not exist, it will be created
     *        e.g. ["/confdConfig/cli/ssh/ip" : "0.0.0.0", "/confdConfig/cli/ssh/ip" : 14022]
     * @param newConfFile (if null original file is modified), otherwise it is copied
     */
    def editConf(String remoteConfFile, String ns, Map params, String newConfFile = null) {
        logger.debug "==> editConf remoteConfFile={} ns={} params={} newConfFile={}", remoteConfFile, ns,
            params, newConfFile

        String prefix = "conf"
        String file = remoteConfFile
        if (newConfFile) {
            runCommand("cp ${remoteConfFile} ${newConfFile}")
            file = newConfFile
        }
        def xmlOpt = "-N ${prefix}=\"${ns}\""
        def xmlOptSel = "sel ${xmlOpt} -t -v"
        def xmlOptEd = "ed -L -O ${xmlOpt}"

        params.each { Map.Entry<Object, Object> entry ->
            List pathList = entry.key.split("/")[1..-1]
            logger.debug "entry.key={} pathList={}", entry.key, pathList
            for (int i = 1; i < pathList.size(); i++) {
                def path = makePrefixedSlashPath(pathList[0..i], prefix)
                def path0 = makePrefixedSlashPath(pathList[0..i - 1], prefix)
                logger.debug "path={}", path
                runCommand("if [ \"\$(xmlstarlet ${xmlOptSel} \"count(${path})\" ${file})\" = \"0\" ]; then " +
                    "xmlstarlet ${xmlOptEd} -s ${path0} -t elem -n ${pathList[i]} ${file}; " + "fi"
                )
            }
            def path = makePrefixedSlashPath(pathList, prefix)
            runCommand("xmlstarlet ${xmlOptEd} -u \"${path}\" -v ${entry.value} ${file}")
        }

        logger.debug "<== editConf"
    }


    /**
     * Edit (optionally into new copy) existing confdConf file on remote
     * @see editConf
     */
    def editConfdConf(String remoteConfDConfFile, Map params, String newConfDConfFile = null) {
        logger.debug "==> editConfdConf remoteConfDConfFile={} params={} newConfDConfFile={}", remoteConfDConfFile,
            params, newConfDConfFile

        editConf(remoteConfDConfFile, "http://tail-f.com/ns/confd_cfg/1.0", params, newConfDConfFile)

        logger.debug "<== editConfdConf"
    }

    /**
     * Edit (optionally into new copy) existing nsoConf file on remote
     * @see editConf
     */
    def editNsoConf(String remoteNsoConfFile, Map params, String newNsoConfFile = null) {
        logger.debug "==> editConfdConf remoteNsoConfFile={} params={} newNsoConfFile={}", remoteNsoConfFile,
            params, newNsoConfFile

        editConf(remoteNsoConfFile, "http://tail-f.com/yang/tailf-ncs-config", params, newNsoConfFile)

        logger.debug "<== editNsoConf"
    }

    def copyConfdConf(String localConfDConfFile, String dir) {
        logger.debug "==> copyConfdConf localConfDConfFile={} dir={}", localConfDConfFile, dir
        if (localConfDConfFile) {
            def filename = new java.io.File(localConfDConfFile).getName()
            copyFilesToContext([localConfDConfFile])
            runCommand "mv ${dir}/confd.conf ${dir}/confd.conf.orig"
            copyFile filename, "${dir}/confd.conf"
        }
        logger.debug "<== copyConfdConf"
    }


    def exampleSupervisor(String path, String confdConf = null) {
        logger.info "==> exampleSupervisor path={} confdConf={}", path, confdConf
        packages += confdBuild
        def examplePath = "${confdDir}/examples.confd/${path}"
        exampleConfdConf = confdConf
        def ret = [program: "confd_example", command: "make -C ${examplePath} start",
                   params : ["environment=PYTHONPATH=\"/${confdDir}/src/confd/pyapi\""]]
        //python examples need PYTHONPATH
        logger.info "<== exampleSupervisor ret={}", ret
        return ret
    }


    def buildExample(String path) {
        logger.info "==> buildExample path={}", path
        packages += confdBuild
        exampleToBuild = path
        logger.info "<== buildExample"
    }

    private def handleConfD() {
        logger.info "==> handleConfD"
        if (confd != false) {
            assert confd instanceof Map

            def confdVersion = confd?.version ?: getProp(CONFD_VERSION_TF, DEFAULT_CONFD_VERSION)
            def confdPackageDir = package_dir
            confdPackageDir = getProp(CONFD_PACKAGE_DIR_TF, confdPackageDir)
            confdPackageDir = confd?.package_dir ?: confdPackageDir
            if (confd?.package_dir == false) { // skip copying (e.g. for tests)
                confdPackageDir = false
            }

            def confdBasic = confd?.basic ?: getPropTrueFalse(CONFD_BASIC_TF, false)
            def confdArch = "x86_64" // TODO from confd
            def confdPlatform = "linux" // TODO from confd
            def ports = [2022, 2023, 2024, 4565, 8008, 8088] // TODO from confd

            def confdPrefix = "confd-${confdBasic ? "basic-" : ""}${confdVersion}"
            def confdFiles = ["${confdPrefix}.${confdPlatform}.${confdArch}.installer.bin",]
            if (confdExamples) {
                confdFiles += ["${confdPrefix}.examples.tar.gz"]
            }
            if (confdJava) {
                confdFiles += ["${confdPrefix}.java.tar.gz"]
            }
            if (confdDoc) {
                confdFiles += ["${confdPrefix}.doc.tar.gz"]
            }
            logger.info "confdPrefix=$confdPrefix confdPackageDir=$confdPackageDir"
            environmentVariable "CONFD_DIR", confdDir
            environmentVariable "PATH", '$CONFD_DIR/bin:$PATH'
            confdFiles.each { file ->  // source is relative to dockerfile location  (context dir)
                copyFile file, "/tmp"
            }
            runCommand "chmod a+x /tmp/*.bin"
            runCommand "/tmp/${confdFiles[0]} \$CONFD_DIR" // first confdFile is installer
            runCommand "chmod -R a+wr  \${CONFD_DIR}/var"
            if (confd?.sshFix) { //for Ubuntu >= 19.04 and confd < 7.2
                runCommand 'rm ${CONFD_DIR}/etc/confd/ssh/ssh_host_*sa_key*'
                runCommand 'ssh-keygen -v -f ${CONFD_DIR}/etc/confd/ssh/ssh_host_rsa_key -N "" -t rsa -m pem'
            }
            runCommand "chmod a+r  \${CONFD_DIR}/etc/confd/ssh/ssh_host_*_key"
            if (confd?.localConfdConf) {
                runCommand "chmod -R a+wr \${CONFD_DIR}/etc/confd"
                copyConfdConf(confd.localConfdConf, "${confdDir}/etc/confd")
            }
            if (confdExamples) {
                runCommand "find \${CONFD_DIR}/examples.confd/ -type d -exec chmod 777 {} +"
                //runCommand "chmod -R a+wr  \${CONFD_DIR}/examples.confd"
                if (exampleToBuild != null) {
                    def exampleDir = "${confdDir}/examples.confd/${exampleToBuild}"
                    workingDir exampleDir
                    runCommand "make clean all"
                    copyConfdConf(exampleConfdConf, exampleDir)
                }
            }
            runCommand "echo 'export CONFD_DIR=${confdDir}' >> ${setupTailfFile}"
            runCommand "echo 'source \${CONFD_DIR}/confdrc' >> ${setupTailfFile}"

            exposePort(project.provider(new Callable() { // ports on one line
                List call() throws Exception {
                    ports
                }
            }))
            if (confdPackageDir) {
                copyFilesToContext(confdFiles.collect { "$confdPackageDir/$it" })
            }
        }
        logger.info "<== handleConfD"
    }

    private def handleNso() {
        logger.info "==> handleNso"
        if (nso != false) {
            assert nso instanceof Map
            def nsoVersion = nso?.version ?: getProp(NSO_VERSION_TF, DEFAULT_NSO_VERSION)
            def nsoPackageDir = package_dir
            nsoPackageDir = getProp(NSO_PACKAGE_DIR_TF, nsoPackageDir)
            nsoPackageDir = nso?.package_dir ?: nsoPackageDir
            if (nso?.package_dir == false) { // skip copying (e.g. for tests)
                nsoPackageDir = false
            }

            def nsoArch = "x86_64" // TODO from nso
            def nsoPlatform = "linux" // TODO from nso
            def ports = [2022, 2023, 2024, 4565, 8008, 8088]

            def nsoPrefix = "nso-${nsoVersion}"
            def nsoFiles = ["${nsoPrefix}.${nsoPlatform}.${nsoArch}.installer.bin",]

            logger.info "nsoPrefix=$nsoPrefix nsoPackageDir=$nsoPackageDir"
            environmentVariable "NCS_DIR", nsoDir
            environmentVariable "PATH", '$NCS_DIR/bin:$PATH'
            nsoFiles.each { file ->  // source is relative to dockerfile location  (context dir)
                copyFile file, "/tmp"
            }
            runCommand "chmod a+x /tmp/*.bin"
            runCommand "/tmp/${nsoFiles[0]} \$NCS_DIR" // first confdFile is installer
            runCommand "chmod -R a+wr  \${NCS_DIR}/var"
            if (nso?.sshFix) { //only for Ubuntu >= 19.04 and Nso < 5.2
                runCommand 'rm ${NCS_DIR}/etc/ncs/ssh/ssh_host_*sa_key*'
                runCommand 'ssh-keygen -v -f ${NCS_DIR}/etc/ncs/ssh/ssh_host_rsa_key -N "" -t rsa -m pem'
            }
            runCommand "chmod a+r  \${NCS_DIR}/etc/ncs/ssh/ssh_host_*_key"
            runCommand "echo 'export NCS_DIR=${nsoDir}' >> ${setupTailfFile}"
            runCommand "echo 'source \${NCS_DIR}/ncsrc' >> ${setupTailfFile}"

            exposePort(project.provider(new Callable() { // ports on one line
                List call() throws Exception {
                    ports
                }
            }))
            if (nsoPackageDir) {
                copyFilesToContext(nsoFiles.collect { "$nsoPackageDir/$it" })
            }
        }
        logger.info "<== handleNso"
    }

    private def handleLux() {
        logger.info "==> handleLux"
        if (installLux != false) {
            def luxDir = "/tmp/lux"
            runCommand "git clone https://github.com/hawk/lux.git ${luxDir}"
            workingDir luxDir
            runCommand "autoconf"
            runCommand "./configure"
            runCommand "make"
            runCommand "make install"
        }
        logger.info "<== handleLux"
    }

    protected void taskHookLast() {
        logger.info "==> taskHookLast"
        if (!packages.contains("python-paramiko")) { // set python3 as default python if python2 not used
            runCommand "update-alternatives --install /usr/bin/python python /usr/bin/python3 2"
        } else {
            runCommand "update-alternatives --install /usr/bin/python python /usr/bin/python2 2"
        }
        handleLux() // lux must be installed before NSO (and maybe ConfD) not to interfere with NSO erlang
        handleConfD()
        handleNso()
        logger.info "<== taskHookLast"
    }

    @TaskAction
    void create() {
        logger.info "==> create"
        if (xenialSecurity) { // add xenial-security repo for libssl1.0.0, if requested
            runCommand "echo \"deb http://security.ubuntu.com/ubuntu xenial-security main\" >> /etc/apt/sources.list"
        }
        if (installLux ||  getPropTrueFalse(INSTALL_LUX_TF, false)) {
            installLux = true
            packages +=luxDeps
        }
        if (confd != false) {
            assert confd instanceof Map
            confdExamples = confd?.examples ?: getPropTrueFalse(CONFD_EXAMPLES_TF, true)
            logger.debug("confdExamples={}", confdExamples)
            confdJava = confd?.java ?: getPropTrueFalse(CONFD_JAVA_TF, false)
            logger.debug("confdJava={}", confdJava)
            confdDoc = confd?.doc ?: getPropTrueFalse(CONFD_DOC_TF, true)
            logger.debug("confdDoc={}", confdDoc)
            packages += confdRuntime + nconsoleRuntime
            if (confdExamples) {
                packages += confdBuild  // we need to build examples
            }
        }

        if (nso != false) {
            assert nso instanceof Map
            packages += nsoRuntime + nconsoleRuntime
        }

        super.create()
        logger.info "<== create"
    }
}
